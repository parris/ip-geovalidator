# docker build --tag geolocator --rm .
# docker run --rm --name geolocate-tests geolocator go test
# docker run -p 8080:8080 --rm --name geolocate geolocator
# docker run -v $(pwd)/main.go:/go/main.go -v $(pwd)/src/geolocate:/go/src/geolocate -p 8080:8080 --rm --name geolocate geolocator
FROM golang:1.10

COPY *.go ./
COPY src/geolocate src/geolocate

RUN go get "github.com/oschwald/maxminddb-golang"
RUN wget https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz \
    && mkdir -p GeoLite2-Country \
    && tar -xvf GeoLite2-Country.tar.gz -C GeoLite2-Country  --strip-components=1

CMD go run main.go