# IP country checker 

## Usage

To run this endpoint as container

```bash
    docker build --tag geolocator --rm .
    docker run -p 8080:8080 --rm --name geolocate geolocator
```

## Tests

To run the automated tests

```bash
    docker run --rm --name geolocate-tests geolocator go test
```

## HEAD /

Validates whether an `ip` is located within the authorized `country` list.

Returns a status 401 (Unauthorized) if the country is not whitelisted.

### REQUEST

```bash
    HEAD localhost:8080?ip=123.456.789&country=US&country=IE
```

#### Parameters

| Name    | Type   | Notes             |
|---------|--------|-------------------|
| ip      | string | IPv4 address      |
| country | array  | Country ISO codes |

#### cURL 

```bash
    curl --head 'localhost:8080?ip=123.456.789&country=US&country=IE&country=FR'
```

### RESPONSE

Returns an HTTP status 200 if the `ip` is located within the list of 
authorized countries.  

Returns an HTTP 401 if the `ip` is not located withing the list of
authorized countries.

Returns an HTTP 400 if the IPv4 address is invalid.

## GET /

Validates whether an `ip` is located within the authorized `country` list.

Returns a status 200 regardless of whether or not the country is authorized.

### REQUEST

```bash
    HEAD localhost:8080?ip=123.456.789&country=US&country=IE
```

#### Parameters

| Name    | Type   | Notes             |
|---------|--------|-------------------|
| ip      | string | IPv4 address      |
| country | array  | Country ISO codes |

#### cURL 

```bash
    curl -v 'http://localhost:8080?ip=123.456.789&country=US&country=IE&country=FR'
```

### RESPONSE

Returns the matched country ISO code if the IPv4 is whitelisted

```json
    {
        "country": "US",
        "error":  "" 
    }
```

Returns an empty string for `country` if the IPv4 address is no whitelisted.

```json
    {
        "country": "",
        "error":  "" 
    }
```

Returns an error message if there was an exception

```json
    {
        "country": "",
        "error":  "invalid IPv4 address" 
    }
```

## Notes

* Generally speaking I would not have added the superfluous GET endpoint.
I supposed that YAGNI doesn't really apply with a coding exercise.
* You can extract a Linux targeted binary using the Docker command

```bash
    docker run -v $(pwd):/go/ geolocator go build
```

* Building the Docker image pulls a fresh `GeoLite2` database.  This 
keeps the mapping data relatively up to date. _(assumes the image
is rebuild periodically and the folks at maxmind don't change the download)_
* A gitlab CI/CD yaml is included - all it does is run tests then 
push the built image to ECS.  