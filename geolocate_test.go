package main

import (
	"geolocate"
	"net"
	"testing"
)

func TestUnitedStatesIP(t *testing.T) {
	ip := net.ParseIP("73.131.17.81")
	err, country := geolocate.GetIpCountry(ip)

	if err != nil {
		t.Errorf("TestUnitedStatesIP returned an error.\n%s", err)
	}

	if country != "US" {
		t.Errorf("TestUnitedStatesIP returned '%s' in error.", country)
	}
}

func TestUnitedKingdomIP(t *testing.T) {
	ip := net.ParseIP("81.2.69.142")
	err, country := geolocate.GetIpCountry(ip)

	if err != nil {
		t.Errorf("TestUnitedKingdomIP returned an error.\n%s", err)
	}

	if country != "GB" {
		t.Errorf("TestUnitedKingdomIP returned '%s' in error.", country)
	}
}

func TestInvalidIP(t *testing.T) {
	ip := net.ParseIP("123.456.789.101")
	err, country := geolocate.GetIpCountry(ip)

	if err == nil {
		t.Error("TestInvalidIP did not an error when it should have.")
	}

	if country != "" {
		t.Errorf("TestInvalidIP returned '%s' in error.", country)
	}
}

func TestValidateCountryMatch(t *testing.T) {
	country := "US"
	whiteList := []string{"US", "FR", "WhoCares"}

	matched := geolocate.ValidateCountry(country, whiteList)

	if !matched {
		t.Error("ValidateCountry did not return US when it should have.")
	}
}

func TestValidateCountryNoMatch(t *testing.T) {
	country := "IE"
	whiteList := []string{"US", "FR", "WhoCares"}

	matched := geolocate.ValidateCountry(country, whiteList)

	if matched {
		t.Error("ValidateCountry found a match when it shouldn't have.")
	}
}
