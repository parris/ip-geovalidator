package main

import (
	"log"
	"net"

	"encoding/json"
	"net/http"

	"errors"
	"geolocate"
	"io"
)

type Response struct {
	Country string `json:"country"`
	Error   string `json:"error"`
}

// handleRoot is the router for the base URL "/"
func handleRoot(w http.ResponseWriter, r *http.Request) {
	switch method := r.Method; method {
	// Handle GET requests
	case "GET":
		var response Response
		err, country := handleRootGet(w, r)

		if err != nil {
			log.Printf("GET error: \n%s", err)
			response = Response{"", err.Error()}
		} else {
			response = Response{country, ""}
		}

		js, err := json.Marshal(response)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("Error building response: %s", err)
		}

		w.Write(js)
	// Handle HEAD requests
	case "HEAD":
		err := handleRootHead(w, r)

		if err != nil {
			log.Printf("HEAD error: \n%s", err)
		}
	// Don't handle other type requests
	default:
		w.WriteHeader(http.StatusNotFound)
		io.WriteString(w, "This URL only supports GET and HEAD requests")
	}
}

// handleRootGet handles GET http requests to the root URL
// In this case the requester inspects the response body to determine whether or not the IP is whitelisted, all valid
// requests will return status 200 with Content-Type: application/json
func handleRootGet(w http.ResponseWriter, r *http.Request) (error, string) {
	// Get query string parameters
	ipParam := r.URL.Query().Get("ip")
	countryParam := r.URL.Query()["country"]

	// Set content-type response headers to application/json
	w.Header().Set("Content-Type", "application/json")

	// Parse the IPv4 address
	ip := net.ParseIP(ipParam)

	if ip == nil {
		w.WriteHeader(http.StatusBadRequest)
		return errors.New("invalid IPv4 address"), ""
	}

	// Get the country associated with the IPv4 address
	err, matchedCountry := geolocate.GetIpCountry(ip)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return errors.New("internal error"), ""
	}

	// Check if the IPv4 address is included in the whitelist
	if geolocate.ValidateCountry(matchedCountry, countryParam) {
		w.WriteHeader(http.StatusOK)
		return nil, matchedCountry
	}

	// 401 if the IPv4 was not in the white list
	w.WriteHeader(http.StatusOK)
	return nil, ""
}

// handleRootGet handles HEAD http requests to the root URL
// In this case the requester expects a return status of 401 (Unauthorized) if the IP address is not located w/in
// a whitelisted country, or a return status of 200 if the country is whitelisted.
func handleRootHead(w http.ResponseWriter, r *http.Request) error {
	// Get query string parameters
	ipParam := r.URL.Query().Get("ip")
	countryParam := r.URL.Query()["country"]

	// Parse the IPv4 address
	ip := net.ParseIP(ipParam)

	if ip == nil {
		w.WriteHeader(http.StatusBadRequest)
		return errors.New("invalid IPv4 address")
	}

	// Get the country associated with the IPv4 address
	err, matchedCountry := geolocate.GetIpCountry(ip)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Print(err)
		return errors.New("internal error")
	}

	// Check if the IPv4 address is included in the whitelist
	if geolocate.ValidateCountry(matchedCountry, countryParam) {
		w.WriteHeader(http.StatusOK)
		return nil
	}

	// 401 if the IPv4 was not in the white list
	w.WriteHeader(http.StatusUnauthorized)
	return nil
}

func main() {
	http.HandleFunc("/", handleRoot)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}
