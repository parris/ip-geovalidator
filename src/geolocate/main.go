package geolocate

import (
	"errors"
	"github.com/oschwald/maxminddb-golang"
	"net"
)

type Response struct {
	Matched        int      `json:"matched"`
	MatchedCountry string   `json:"matchedCountry"`
	Errors         []string `json:"errors"`
}

// GetIpCountry returns the country ISO code for a given IPv4 address
func GetIpCountry(ip net.IP) (error, string) {
	if ip == nil {
		return errors.New("invalid IPv4 address"), ""
	}

	var record struct {
		Country struct {
			ISOCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
	}

	// Get a GeoLite2-Country db handle
	db, err := maxminddb.Open("/go/GeoLite2-Country/GeoLite2-Country.mmdb")
	if err != nil {
		return err, ""
	}
	defer db.Close()

	// Lookup the IP's country ISOCode
	err = db.Lookup(ip, &record)
	if err != nil {
		return err, ""
	}

	return nil, record.Country.ISOCode
}

// ValidateCountry searches array `countries` for parameter `country`
func ValidateCountry(country string, countries []string) bool {
	for i := range countries {
		if countries[i] == country {
			return true
		}
	}

	return false
}
